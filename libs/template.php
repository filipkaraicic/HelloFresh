<?php

/**
 * Custom templating engine for rendering html content
 * Class Template
 * @author fkaraicic
 */
class Template {
    protected $path, $data;

    public function __construct( $data = array(), $path = 'views/layout.php') {
        $this->path = 'templates/' . $path;
        $this->data = $data;
    }

    /**
     * Render html to string
     * @return string
     */
    public function render() {
        if(file_exists($this->path)){

            //Lets extract vars to skip "this"
            extract($this->data);

            //Starts output buffering
            ob_start();

            //Includes template
            include $this->path;
            $buffer = ob_get_contents();
            @ob_end_clean();

            //Returns output buffer
            return $buffer;
        } else {
            return 'Error: Template not found.';
        }
    }

    /**
     * Display page
     */
    public function show() {
        $page =  $this->render();
        echo $page;
    }
}

?>