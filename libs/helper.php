<?php

/**
 * Helper library holding together all our helper functions in one place
 * Class Helper
 * @author fkaraicic
 */
class Helper {

    public static function parseURL(){
        $pathinfo = $_SERVER['REQUEST_URI'];
        $action = substr($pathinfo, 1);

        if (empty($action))
            $action = 'home';

        return $action;
    }

}

?>