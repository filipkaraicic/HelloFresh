<?php

/**
 * Class HomeController
 */
class HomeController {

    public function index() {
        $data = array();
        $data['view'] = 'home';
        $tmpl = new Template($data);
        $tmpl->show();
    }

    /**
     * Custom 404 page
     */
    public function pageNotFound() {
        $data = array();
        $data['view'] = 'home';
        $data['info'] = 'Sorry seems that this page does not exist.';
        $tmpl = new Template($data);
        $tmpl->show();
    }
}

?>