<?php

/**
 * Class LoginController
 */
class LoginController {

    private $errors;

    public function index() {
        $data = array();
        $data['view'] = 'login';
        if (!empty($_SESSION['errors'])) {
            $data['errors'] = $_SESSION['errors'] ;
            $_SESSION['errors'] = array();
        }
        $tmpl = new Template($data);
        $tmpl->show();
    }

    public function loginAction() {
        if($this->validate()) {
            $email = $_POST['email'];
            $user = User::getUser($email);
            if (!empty($user)){
                if ( strcmp($user->password, crypt($_POST['password'], $user->password)) == 0) {
                    //Success, lets login user
                    $_SESSION['name'] = $user->name;
                    $_SESSION['userId'] = $user->id;
                    $data = array();
                    $data['view'] = 'profile';
                    $data['name'] = $user->name;
                    $tmpl = new Template($data);
                    $tmpl->show();
                } else {
                    //Wrong username and pass combination
                    $_SESSION['errors']['password'][] = "Error logging you in.";
                    $this->index();
                }
            } else {
                //Non existing user.
                $_SESSION['errors']['password'][] = "Error logging you in.";
                $this->index();
            }
        } else {
            // Validation errors
            $_SESSION['errors'] = $this->errors;
            $this->index();
        }
    }

    private function validate() {
        $v = new Valitron\Validator($_POST);
        $v->rule('required', ['email', 'password']);
        $v->rule('email', 'email');
        $result = $v->validate();
        $this->errors = $v->errors();
        return $result;

    }

    public function logout() {
        session_destroy();
        $home = new HomeController();
        $home->index();
    }
}

?>