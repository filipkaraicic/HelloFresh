<?php

/**
 * Class SearchController
 */
class SearchController {

    public function index() {
        if ( empty($_SESSION['userId'])){
            $this->loginFirst();
        } else {
            $keyword = $_POST['keyword'];
            $users = User::search($keyword);
            $data = array();
            $data['view'] = 'search';
            $data['users'] = $users;
            $tmpl = new Template($data);
            $tmpl->show();
        }

    }

    private function loginFirst(){
        $data = array();
        $data['view'] = 'login';
        $data['info'] = 'Please login.';
        if (!empty($_SESSION['errors'])) {
            $data['errors'] = $_SESSION['errors'] ;
            $_SESSION['errors'] = array();
        }
        $tmpl = new Template($data);
        $tmpl->show();
    }
}

?>