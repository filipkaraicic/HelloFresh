<?php

/**
 * Class RegisterController
 */
class RegisterController {

    private $errors;

    public function index() {
        $data = array();
        $data['view'] = 'register';
        if (!empty($_SESSION['errors'])) {
            $data['errors'] = $_SESSION['errors'] ;
            $_SESSION['errors'] = array();
        }
        $tmpl = new Template($data);
        $tmpl->show();
    }

    public function addUser() {
        if($this->validate()) {
            $data = array();
            $data['view'] = 'registerResult';
            $email = $_POST['email'];
            $name = $_POST['name'];
            $password = $_POST['password'];

            //First check if user already exist
            if(User::userExists($email)) {
                $data['error'] = 'There is already user with that email address.';
            } else {
                if (User::addUser($name, $email, $password)) {
                    $data['success'] = "Thanks for your registration.";
                } else {
                    $data['error'] = 'Sorry something went wrong';
                }
            }
            $tmpl = new Template($data);
            $tmpl->show();
        } else {
            // Validation errors
            $_SESSION['errors'] = $this->errors;
            $this->index();
        }

    }

    private function validate() {
        $v = new Valitron\Validator($_POST);
        $v->rule('required', ['name', 'email', 'password', 'confirmPassword']);
        $v->rule('email', 'email');
        $v->rule('equals', 'password', 'confirmPassword');
        $result = $v->validate();
        $this->errors = $v->errors();
        return $result;

    }

}

?>