<?php
/**
 * Entry point for whole project.
 * All requests are redirected to this router file.
 *
 */
session_start();

require_once('vendor/autoload.php');
require_once('libs/helper.php');
require_once('libs/template.php');

require_once('config/config.php');

//Get the params from url
$action = Helper::parseURL();

//Router
switch ($action) {
    case 'home':
        $controller = new HomeController();
        $controller->index();
        break;
    case 'register':
        $controller = new RegisterController();
        $controller->index();
        break;
    case 'addUser':
        $controller = new RegisterController();
        $controller->addUser();
        break;
    case 'login':
        $controller = new LoginController();
        $controller->index();
        break;
    case 'loginAction':
        $controller = new LoginController();
        $controller->loginAction();
        break;
    case 'search':
        $controller = new SearchController();
        $controller->index();
        break;
    case 'logout':
        $controller = new LoginController();
        $controller->logout();
        break;
    default:
        $controller = new HomeController();
        $controller->pageNotFound();
        break;


}



?>