<?php

/**
 * Class dbConn
 */
class DB {

    //This should be filled with credentials for production server
    private static $USERNAME = "";
    private static $PASSWORD = "";
    private static $HOST = "";
    private static $DBNAME = "";

    //variable to hold connection object.
    protected static $db;

    //private construct - class cannot be instantiated externally.
    private function __construct() {

        try {
            // assign PDO object to db variable
            self::$db = new PDO( "mysql:host=" . Self::$HOST . ";dbname=" . Self::$DBNAME . "", Self::$USERNAME, Self::$PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'") );
            self::$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        }
        catch (PDOException $e) {
            //Output error - would normally log this to error file rather than output to user.
            echo "Connection Error: " . $e->getMessage();
        }

    }

    // get connection function. Static method - accessible without instantiation
    public static function getConnection() {

        //Guarantees single instance, if no connection object exists then create one.
        if (!self::$db) {
            //new connection object.
            new DB();
        }

        //return connection.
        return self::$db;
    }

}//end class

?>