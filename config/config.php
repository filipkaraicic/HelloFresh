<?php

function detectEnv(){
    if (file_exists( __DIR__ . '/environment.php')) {
        $env = include __DIR__ . '/environment.php';
        return '/' . $env . "/";
    } else {

        return "/";
    }
}

$env = detectEnv();
require_once( __DIR__ . $env . "DB.php");


?>