To start application you will need:

1) MySQL database (create script located in /database)
2) This PHP Codebase

To configure application for local environment:

1) create new file in config directory named environment.php with this content:

<?php

return 'local';

?>
(Instead of local you can put any name you want for your environment.)

2) After that create new directory inside config directory named "local" (or whatever you named your environment)

3) Copy these files into your new directory and modify to match your environment credentials and desired settings:

    1) DB.php



Enjoy the app!