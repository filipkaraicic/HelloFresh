<?php

/**
 * User model for business logic and communication with database.
 * Class User
 */
class User {

    /**
     * Check if user with that email already exists in database
     * @param $email
     * @return string
     */
    public static function userExists($email) {
        $pdo = DB::getConnection();
        $sql = 'SELECT COUNT(*) from user WHERE email = :email';
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetchColumn();
    }

    /**
     * Create new user.
     * @param $name
     * @param $email
     * @param $password
     * @return bool
     */
    public static function addUser($name, $email, $password) {
        $password = Self::hashPass($password);
        $pdo = DB::getConnection();
        $sql = "INSERT INTO user (name, email, password) VALUES (:name, :email, :password)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $result = $stmt->execute();
        return $result;
    }

    /**
     * Hash password with dynamic salt for safe db storage.
     * @param $password
     * @return string
     */
    private static function hashPass($password) {
        $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
        $salt = sprintf("$2a$%02d$", 10) . $salt;
        $hash = crypt($password, $salt);
        return $hash;
    }

    /**
     * Retrieve user from database
     * @param $email
     * @return mixed
     */
    public static function getUser($email){
        $pdo = DB::getConnection();
        $sth = $pdo->prepare('SELECT * FROM user WHERE email = :email LIMIT 1');
        $sth->bindParam(':email', $email);
        $sth->execute();
        $user = $sth->fetch(PDO::FETCH_OBJ);
        return $user;
    }

    /**
     * Search users by keyword (show new users first)
     * @param $keyword
     * @return array
     */
    public static function search($keyword) {
        $pdo = DB::getConnection();
        $sth = $pdo->prepare('SELECT * FROM user WHERE email LIKE ? OR name LIKE ? ORDER BY ID DESC');
        $keyword = "%" . $keyword . "%";
        $sth->bindParam(1, $keyword, PDO::PARAM_STR);
        $sth->bindParam(2, $keyword, PDO::PARAM_STR);
        $sth->execute();
        $users = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $users;
    }
}

?>