<form method="post" action="/loginAction" class="form-horizontal">
    <fieldset>
        <legend>Login</legend>
        <div class="form-group <?php if (!empty($errors['email'])) echo 'has-error'; ?>">
            <label for="inputEmail" class="col-lg-2 control-label">Email</label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="inputEmail" name="email" placeholder="* Enter your email address" value="<?php echo isset($_POST['email']) ? htmlspecialchars($_POST['email']) : '' ?>">
                <?php if (!empty($errors['email'])): ?>
                    <?php foreach ($errors['email'] as $index=>$msg): ?>
                        <label class="control-label" for="inputEmail"><?= $msg ?>. </label>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group <?php if (!empty($errors['password'])) echo 'has-error'; ?>">
            <label for="inputPassword" class="col-lg-2 control-label">Password</label>
            <div class="col-lg-10">
                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="* Enter your password">
                <?php if (!empty($errors['password'])): ?>
                    <?php foreach ($errors['password'] as $index=>$msg): ?>
                        <label class="control-label" for="inputEmail"><?= $msg ?>. </label>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary">Login</button>
            </div>
        </div>
    </fieldset>
</form>