<html>
<head>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/templates/styles/main.css">
    <link rel="stylesheet" href="/libs/superhero.min.css">
    <title>Filip</title>


</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Filip Karaicic</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <li class=""><a href="/register">Register</a></li>
                <li><a href="/login">Login</a></li>
            </ul>
            <form class="navbar-form navbar-left" role="search" method="post" action="/search">
                <div class="form-group">
                    <input type="text" class="form-control" name="keyword" placeholder="* Email | Name">
                </div>
                <button type="submit" class="btn btn-default">Search</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<div id="mainContent" class="container">
    <div class="row">
        <div id="alerts" class="">
            <?php if (!empty($info)): ?>
                <div class="alert alert-dismissible alert-info">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Info!</strong><br>  <?= $info ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($error)): ?>
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Oh snap!</strong><br> <?= $error ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($success)): ?>
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Well done!</strong> <br> <?= $success ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php include_once($view . ".php"); ?>
</div>
</body>
</html>


