<?php if (!empty($users)): ?>
    <table class="table table-striped table-hover ">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>

            <?php foreach($users as $index=>$user): ?>
            <tr>
                <td><?= $user['id'] ?></td>
                <td><?= $user['name'] ?></td>
                <td><?= $user['email'] ?></td>
            </tr>
           <?php endforeach; ?>


        </tbody>
    </table>
<?php else: ?>
    <div class="jumbotron">
        <h1>Sorry, no users found</h1>
        <p>You can register new user by clicking on register button below..</p>
        <p><a class="btn btn-primary btn-lg" href="/register">Register</a></p>
    </div>
<?php endif; ?>