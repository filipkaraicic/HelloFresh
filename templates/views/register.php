<form method="post" action="/addUser" class="form-horizontal">
    <fieldset>
        <legend>Register new user</legend>
        <div class="form-group <?php if (!empty($errors['email'])) echo 'has-error'; ?>">
            <label for="inputEmail" class="col-lg-2 control-label">Email</label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="inputEmail" name="email" placeholder="* Enter your email address" value="<?php echo isset($_POST['email']) ? htmlspecialchars($_POST['email']) : '' ?>">
                <?php if (!empty($errors['email'])): ?>
                    <?php foreach ($errors['email'] as $index=>$msg): ?>
                <label class="control-label" for="inputEmail"><?= $msg ?>. </label>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group <?php if (!empty($errors['name'])) echo 'has-error'; ?>">
            <label for="inputName" class="col-lg-2 control-label">Name</label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="inputName" name="name" placeholder="* Enter your full name" value="<?php echo isset($_POST['name']) ? htmlspecialchars($_POST['name']) : '' ?>">
                <?php if (!empty($errors['name'])): ?>
                    <?php foreach ($errors['name'] as $index=>$msg): ?>
                        <label class="control-label" for="inputEmail"><?= $msg ?>. </label>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group <?php if (!empty($errors['password'])) echo 'has-error'; ?>">
            <label for="inputPassword" class="col-lg-2 control-label">Password</label>
            <div class="col-lg-10">
                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="* Select password">
                <?php if (!empty($errors['password'])): ?>
                    <?php foreach ($errors['password'] as $index=>$msg): ?>
                        <label class="control-label" for="inputEmail"><?= $msg ?>. </label>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group <?php if (!empty($errors['confirmPassword'])) echo 'has-error'; ?>">
            <label for="inputPassword" class="col-lg-2 control-label">Confirm Password</label>
            <div class="col-lg-10">
                <input type="password" class="form-control" id="inputPasswordConfirm" name="confirmPassword" placeholder="* Re-type same password">
                <?php if (!empty($errors['confirmPassword'])): ?>
                    <?php foreach ($errors['confirmPassword'] as $index=>$msg): ?>
                        <label class="control-label" for="inputEmail"><?= $msg ?>. </label>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-default">Reset</button>
            </div>
        </div>
    </fieldset>
</form>